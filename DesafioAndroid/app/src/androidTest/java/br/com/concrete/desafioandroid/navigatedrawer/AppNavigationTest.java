package br.com.concrete.desafioandroid.navigatedrawer;

import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.Gravity;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import br.com.concrete.desafioandroid.R;
import br.com.concrete.desafioandroid.repository.RepositoryActivity;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.DrawerMatchers.isClosed;
import static android.support.test.espresso.contrib.DrawerMatchers.isOpen;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class AppNavigationTest {

    @Rule
    public ActivityTestRule<RepositoryActivity> poiActivityTestRule =
            new ActivityTestRule<>(RepositoryActivity.class);

    @Test
    public void clickOnAndroidHomeIcon_OpensNavigation() {
        onView(withId(R.id.drawer_layout))
                .check(matches(isClosed(Gravity.LEFT)));

        String navigateUpDesc = poiActivityTestRule.getActivity()
                .getString(android.support.v7.appcompat.R.string.abc_action_bar_up_description);
        onView(withContentDescription(navigateUpDesc)).perform(click());

        onView(withId(R.id.drawer_layout))
                .check(matches(isOpen(Gravity.LEFT)));
    }
}
