package br.com.concrete.desafioandroid.pull;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import br.com.concrete.desafioandroid.R;
import br.com.concrete.desafioandroid.adapter.PullAdapter;
import br.com.concrete.desafioandroid.data.PullServiceEndPoint;
import br.com.concrete.desafioandroid.entity.Pull;
import br.com.concrete.desafioandroid.entity.Repository;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

public class PullActivity extends AppCompatActivity implements PullContract.View {

    private Repository repository;
    private static PullContract.UserActionsListener listener;
    @BindView(R.id.progress)
    ProgressBar progressBar;
    @BindView(R.id.recyclerview_pull)
    RecyclerView recyclerViewPull;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull);
        ButterKnife.bind(this);
        if (getIntent() != null) {
            this.repository = (Repository) getIntent().getSerializableExtra("repository");
            Call<List<Pull>> call = PullServiceEndPoint.getPulls(repository.getName(), repository.getOwner().getLogin());
            listener = new PullPresenter(this, call);
            listener.loadPulls();
        }
        getSupportActionBar().setTitle(repository.getName());

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_navigation_arrow_back);
    }

    @Override
    public void setProgressIndicator(boolean active) {
        if (active) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void showPulls(List<Pull> pulls) {
        PullAdapter repositoryAdapter = new PullAdapter(PullActivity.this, pulls);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(PullActivity.this);
        recyclerViewPull.setLayoutManager(linearLayoutManager);
        recyclerViewPull.setHasFixedSize(true);
        recyclerViewPull.setAdapter(repositoryAdapter);
    }

    @Override
    public void showErro() {
        Toast.makeText(this, "Ocorreu um erro inesperado!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return true;
    }
}
