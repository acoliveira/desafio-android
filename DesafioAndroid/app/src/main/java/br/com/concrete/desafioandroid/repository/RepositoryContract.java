package br.com.concrete.desafioandroid.repository;

import java.util.List;

import br.com.concrete.desafioandroid.entity.Repository;

/**
 * Created by Alan on 23/09/2017.
 */

public interface RepositoryContract {

    interface View {

        void setProgressIndicator(boolean active);

        void showRepositories(List<Repository> repositories);

        void showErro();
    }

    interface UserActionsListener {
        void loadRepositories();
    }
}
