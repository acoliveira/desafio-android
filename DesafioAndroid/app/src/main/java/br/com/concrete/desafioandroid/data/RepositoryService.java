package br.com.concrete.desafioandroid.data;

import br.com.concrete.desafioandroid.entity.BodyRequest;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Alan on 23/09/2017.
 */

public interface RepositoryService {
    @Headers({
            "Content-Type: application/json",
            "User-Agent: https://api.github.com/meta"
    })
    @GET("repositories?q=language:Java&sort=stars")
    Call<BodyRequest> getRepositories(@Query("page") Integer page);
}
