package br.com.concrete.desafioandroid;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;

import br.com.concrete.desafioandroid.repository.RepositoryActivity;

/**
 * Created by alancostaoliveira on 5/20/16.
 */
public class NavigationViewController implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawerLayout;
    private Context context;

    public NavigationViewController(Context context, DrawerLayout drawerLayout) {
        this.context = context;
        this.drawerLayout = drawerLayout;
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        Intent intent = null;
        if (!menuItem.isChecked()) {
            switch (menuItem.getItemId()) {
                case R.id.drawer_repo:
                    intent = new Intent(context, RepositoryActivity.class);
                    break;
                default:
                    break;
            }

            intent.putExtra("menuItemId", menuItem.getItemId());
            drawerLayout.closeDrawers();
            context.startActivity(intent);
        }
        return true;
    }

}
