package br.com.concrete.desafioandroid.data;

import br.com.concrete.desafioandroid.entity.BodyRequest;
import retrofit2.Call;

/**
 * Created by Alan on 23/09/2017.
 */

public class RepositoryServiceEndPoint extends ServiceBase {
    public static Call<BodyRequest> getDataPlace(int page) {
        RepositoryService service = RepositoryServiceEndPoint.createService(RepositoryService.class);
        return service.getRepositories(page);
    }
}
