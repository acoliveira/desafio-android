package br.com.concrete.desafioandroid.pull;

import java.util.List;

import br.com.concrete.desafioandroid.entity.Pull;
import br.com.concrete.desafioandroid.entity.Repository;

/**
 * Created by Alan on 23/09/2017.
 */

public interface PullContract {

    interface View {

        void setProgressIndicator(boolean active);

        void showPulls(List<Pull> pulls);

        void showErro();
    }

    interface UserActionsListener {
        void loadPulls();
    }
}
