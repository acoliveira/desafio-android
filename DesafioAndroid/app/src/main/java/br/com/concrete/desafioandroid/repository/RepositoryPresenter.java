package br.com.concrete.desafioandroid.repository;

import android.support.annotation.NonNull;

import br.com.concrete.desafioandroid.entity.BodyRequest;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.google.gson.internal.$Gson$Preconditions.checkNotNull;

/**
 * Created by Alan on 23/09/2017.
 */

public class RepositoryPresenter implements RepositoryContract.UserActionsListener {

    private RepositoryContract.View view;
    private Call<BodyRequest> call;

    public RepositoryPresenter(@NonNull RepositoryContract.View view, @NonNull Call<BodyRequest> call) {
        this.view = checkNotNull(view);
        this.call = checkNotNull(call);
    }

    @Override
    public void loadRepositories() {
        view.setProgressIndicator(true);
        call.enqueue(new Callback<BodyRequest>() {
            @Override
            public void onResponse(Call<BodyRequest> call, Response<BodyRequest> response) {
                if (response.isSuccessful()) {
                    view.setProgressIndicator(false);
                    view.showRepositories(response.body().getRepositories());
                } else {
                    new Throwable("Falha ao carregar Repositorios");
                }
            }

            @Override
            public void onFailure(Call<BodyRequest> call, Throwable t) {
                view.setProgressIndicator(false);
                view.showErro();
            }
        });
    }
}
