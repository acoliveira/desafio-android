package br.com.concrete.desafioandroid.repository;

import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import br.com.concrete.desafioandroid.NavigationViewController;
import br.com.concrete.desafioandroid.R;
import br.com.concrete.desafioandroid.adapter.RepositoryAdapter;
import br.com.concrete.desafioandroid.data.RepositoryServiceEndPoint;
import br.com.concrete.desafioandroid.entity.BodyRequest;
import br.com.concrete.desafioandroid.entity.Repository;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

public class RepositoryActivity extends AppCompatActivity implements RepositoryContract.View {
    private static RepositoryContract.UserActionsListener listener;
    @BindView(R.id.recyclerview_repositories)
    RecyclerView recyclerViewRepositories;
    @BindView(R.id.progress)
    ProgressBar progressBar;
    @BindView(R.id.navigationView)
    NavigationView navigationView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    private Call<BodyRequest> call;
    private LinearLayoutManager linearLayoutManager;
    private boolean reload = false;
    private int page = 1;
    private RepositoryAdapter repositoryAdapter;
    private NavigationViewController navigationViewController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repository);
        ButterKnife.bind(this);
        bindRepositories();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_navigation_drawer);
        getSupportActionBar().setHomeButtonEnabled(true);

        navigationViewController = new NavigationViewController(this, drawerLayout);
        navigationView.setNavigationItemSelectedListener(navigationViewController);

        MenuItem menuItem = navigationView.getMenu().findItem(R.id.drawer_repo);
        if (menuItem != null) {
            menuItem.setChecked(true);
        }
    }

    public void bindRepositories() {
        call = RepositoryServiceEndPoint.getDataPlace(page);
        listener = new RepositoryPresenter(this, call);
        listener.loadRepositories();
    }

    @Override
    public void setProgressIndicator(boolean active) {
        if (active) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void showRepositories(List<Repository> repositories) {
        reload = false;
        if (page == 1) {
            repositoryAdapter = new RepositoryAdapter(RepositoryActivity.this, repositories);
            linearLayoutManager = new LinearLayoutManager(RepositoryActivity.this);
            recyclerViewRepositories.setLayoutManager(linearLayoutManager);
            recyclerViewRepositories.setHasFixedSize(true);
            recyclerViewRepositories.setAdapter(repositoryAdapter);
            recyclerViewRepositories.addOnScrollListener(onScrollListener());
        } else {
            reload = true;
            repositoryAdapter.add(repositories);
            repositoryAdapter.notifyDataSetChanged();
            reload = false;
        }
    }

    @Override
    public void showErro() {
        Toast.makeText(this, "Ocorreu um erro inesperado!", Toast.LENGTH_SHORT).show();
    }

    private RecyclerView.OnScrollListener onScrollListener() {
        return new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                int visibleItemCount = recyclerView.getChildCount();
                int totalItemCount = linearLayoutManager.getItemCount();
                int firstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();

                if ((firstVisibleItem+visibleItemCount) == totalItemCount && !reload) {
                    page = page + 1;
                    bindRepositories();
                }
            }
        };
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if(drawerLayout != null) {
                    drawerLayout.openDrawer(GravityCompat.START);
                }
                break;
        }
        return true;
    }
}
