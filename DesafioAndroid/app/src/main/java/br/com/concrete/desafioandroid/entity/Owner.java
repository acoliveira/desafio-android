package br.com.concrete.desafioandroid.entity;

/**
 * Created by Alan on 23/09/2017.
 */

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Owner implements Serializable {
    private int id;
    private String login;
    @SerializedName("avatar_url")
    private String avatarUrl;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }
}
