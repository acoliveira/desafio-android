package br.com.concrete.desafioandroid.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.concrete.desafioandroid.R;
import br.com.concrete.desafioandroid.entity.Pull;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alan on 24/09/2017.
 */

public class PullAdapter extends RecyclerView.Adapter<PullAdapter.ViewHolder> {

    private Context context;
    private List<Pull> pulls;

    public PullAdapter(Context context, List<Pull> pulls) {
        this.context = context;
        this.pulls = pulls;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_pull, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Pull pull = pulls.get(position);
        holder.bind(pull);
    }

    @Override
    public int getItemCount() {
        return pulls.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.label_pull_title)
        TextView labelPullTitle;
        @BindView(R.id.label_pull_body)
        TextView labelPullBody;
        @BindView(R.id.label_date)
        TextView labelDate;

        @BindView(R.id.label_user_name)
        TextView labelUserName;
        @BindView(R.id.imageview_user)
        ImageView imageViewUser;

        private Pull pull;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        public void bind(Pull pull) {
            this.pull = pull;
            labelPullTitle.setText(pull.getTitle());
            labelPullBody.setText(pull.getBody());
            labelDate.setText(pull.getCreated_at());
            labelUserName.setText(pull.getUser().getName());
            Picasso.with(context).load(pull.getUser().getAvatarUrl()).into(imageViewUser);
        }

        @Override
        public void onClick(View view) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(pull.getHtmlUrl()));
            context.startActivity(browserIntent);
        }
    }
}
