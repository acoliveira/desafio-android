package br.com.concrete.desafioandroid.entity;

/**
 * Created by Alan on 23/09/2017.
 */
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Repository implements Serializable {
    private int id;
    private String name;
    private String description;
    @SerializedName("forks_count")
    private int forksCount;
    @SerializedName("stargazers_count")
    private int stargazersCount;
    private Owner owner;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getForksCount() {
        return forksCount;
    }

    public void setForksCount(int forksCount) {
        this.forksCount = forksCount;
    }

    public int getStargazersCount() {
        return stargazersCount;
    }

    public void setStargazersCount(int stargazersCount) {
        this.stargazersCount = stargazersCount;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }
}