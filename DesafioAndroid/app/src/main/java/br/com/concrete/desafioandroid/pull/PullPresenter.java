package br.com.concrete.desafioandroid.pull;

import android.support.annotation.NonNull;

import java.util.List;

import br.com.concrete.desafioandroid.entity.BodyRequest;
import br.com.concrete.desafioandroid.entity.Pull;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.google.gson.internal.$Gson$Preconditions.checkNotNull;

/**
 * Created by Alan on 23/09/2017.
 */

public class PullPresenter implements PullContract.UserActionsListener {

    private PullContract.View view;
    private Call<List<Pull>> call;

    public PullPresenter(@NonNull PullContract.View view, @NonNull Call<List<Pull>> call) {
        this.view = checkNotNull(view);
        this.call = checkNotNull(call);
    }

    @Override
    public void loadPulls() {
        view.setProgressIndicator(true);
        call.enqueue(new Callback<List<Pull>>() {
            @Override
            public void onResponse(Call<List<Pull>> call, Response<List<Pull>> response) {
                if (response.isSuccessful()) {
                    view.setProgressIndicator(false);
                    view.showPulls(response.body());
                } else {
                    new Throwable("Falha ao carregar Pull");
                }
            }

            @Override
            public void onFailure(Call<List<Pull>> call, Throwable t) {
                view.setProgressIndicator(false);
                view.showErro();
            }
        });
    }
}
