package br.com.concrete.desafioandroid.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.concrete.desafioandroid.R;
import br.com.concrete.desafioandroid.entity.Repository;
import br.com.concrete.desafioandroid.pull.PullActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alan on 24/09/2017.
 */

public class RepositoryAdapter extends RecyclerView.Adapter<RepositoryAdapter.ViewHolder> {


    private Context context;
    private List<Repository> repositories;

    public RepositoryAdapter(Context context, List<Repository> repositories) {
        this.context = context;
        this.repositories = repositories;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_repository, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Repository repository = repositories.get(position);
        holder.bind(repository);
    }

    public void add(List<Repository> repositories) {
        for (Repository repository:repositories) {
            this.repositories.add(repository);
        }
    }

    @Override
    public int getItemCount() {
        return repositories.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.label_repository_name)
        TextView labelRepositoryName;
        @BindView(R.id.label_descr_respository)
        TextView labelDescrRepository;
        @BindView(R.id.label_number_start)
        TextView labelNumberStar;
        @BindView(R.id.label_number_forks)
        TextView labelNumberForks;
        @BindView(R.id.label_user_name)
        TextView labelUserName;
        @BindView(R.id.imageview_user)
        ImageView imageViewUser;

        private Repository repository;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        public void bind(Repository repository) {
            this.repository = repository;
            labelRepositoryName.setText(repository.getName());
            labelDescrRepository.setText(repository.getDescription());
            labelNumberStar.setText(String.format("Starts: %d", repository.getStargazersCount()));
            labelNumberForks.setText(String.format("Forks: %d", repository.getForksCount()));
            labelUserName.setText(repository.getOwner().getLogin());
            Picasso.with(context).load(repository.getOwner().getAvatarUrl()).into(imageViewUser);
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(context, PullActivity.class);
            intent.putExtra("repository", repository);
            context.startActivity(intent);
        }
    }
}
