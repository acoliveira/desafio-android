package br.com.concrete.desafioandroid.data;

import java.util.List;

import br.com.concrete.desafioandroid.entity.BodyRequest;
import br.com.concrete.desafioandroid.entity.Pull;
import retrofit2.Call;

/**
 * Created by Alan on 23/09/2017.
 */

public class PullServiceEndPoint extends ServiceBase {
    public static Call<List<Pull>> getPulls(String repository, String owner) {
        PullsService service = PullServiceEndPoint.createService(PullsService.class);
        return service.getPulls(repository, owner);
    }
}
