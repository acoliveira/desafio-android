package br.com.concrete.desafioandroid.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Alan on 24/09/2017.
 */

public class User implements Serializable {
    @SerializedName("login")
    private String name;
    @SerializedName("avatar_url")
    private String avatarUrl;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }
}
