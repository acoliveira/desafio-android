package br.com.concrete.desafioandroid.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import butterknife.BindView;

/**
 * Created by Alan on 24/09/2017.
 */

public class Pull implements Serializable {
    private String title;
    private String body;
    private String created_at;
    private User user;
    @SerializedName("html_url")
    private String htmlUrl;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getHtmlUrl() {
        return htmlUrl;
    }

    public void setHtmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
    }
}
