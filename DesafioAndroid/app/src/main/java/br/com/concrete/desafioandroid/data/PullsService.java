package br.com.concrete.desafioandroid.data;

import java.util.List;

import br.com.concrete.desafioandroid.entity.BodyRequest;
import br.com.concrete.desafioandroid.entity.Pull;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Alan on 23/09/2017.
 */

public interface PullsService {
    @Headers({
            "Content-Type: application/json",
            "User-Agent: https://api.github.com/meta"
    })
    @GET("/repos/{owner}/{repository}/pulls")
    Call<List<Pull>> getPulls(@Path("repository") String repository,
                              @Path("owner") String owner);
}
