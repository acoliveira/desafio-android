package br.com.concrete.desafioandroid.repository;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import br.com.concrete.desafioandroid.entity.BodyRequest;
import br.com.concrete.desafioandroid.entity.Owner;
import br.com.concrete.desafioandroid.entity.Repository;
import br.com.concrete.desafioandroid.repository.RepositoryContract;
import br.com.concrete.desafioandroid.repository.RepositoryPresenter;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.mockito.Mockito.verify;

/**
 * Created by Alan on 23/09/2017.
 */
public class RepositoryActivityTest {

    @Mock
    RepositoryContract.View repositoryView;
    @Mock
    Call<BodyRequest> call;
    @Mock
    RepositoryPresenter repositoryPresenter;

    @Captor
    private ArgumentCaptor<Callback<BodyRequest>> callbackArgumentCaptor;
    private BodyRequest bodyRequest;
    private Response<BodyRequest> response;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        repositoryPresenter = new RepositoryPresenter(repositoryView, call);
    }

    @Test
    public void showRepositories() throws Exception {
        repositoryView.setProgressIndicator(true);

        Owner owner = new Owner();
        owner.setId(8074);
        owner.setLogin("jfeinstein10");
        owner.setAvatarUrl("https://avatars1.githubusercontent.com/u/1269143?v=4");

        Repository repository = new Repository();
        repository.setId(4839957);
        repository.setName("SlidingMenu");
        repository.setDescription("An Android library that allows you to easily create applications with slide-in menus. You may use it in your Android apps provided that you cite this project and include the license in your app. Thanks!");
        repository.setForksCount(64445);
        repository.setStargazersCount(884);
        repository.setOwner(owner);

        List<Repository> repositories = new ArrayList<>();
        repositories.add(repository);

        bodyRequest = new BodyRequest();
        bodyRequest.setRepositories(repositories);
        response = Response.success(bodyRequest);

        repositoryPresenter.loadRepositories();

        verify(call).enqueue(callbackArgumentCaptor.capture());
        callbackArgumentCaptor.getValue().onResponse(call, response);

        verify(repositoryView).setProgressIndicator(false);
        verify(repositoryView).showRepositories(response.body().getRepositories());
    }

    @Test
    public void showRepositoryNullPointer() throws NullPointerException {
        repositoryView.showRepositories(null);
    }

    @Test
    public void showErro() throws Exception {
        repositoryView.setProgressIndicator(true);

        MediaType mediaType = MediaType.parse("application/json; charset=utf-8");
        ResponseBody body = ResponseBody.create(mediaType, "Unauthorized");

        response = Response.error(401, body);
        repositoryPresenter.loadRepositories();

        verify(call).enqueue(callbackArgumentCaptor.capture());
        callbackArgumentCaptor.getValue().onFailure(call, new Throwable("Unauthorized"));

        verify(repositoryView).setProgressIndicator(false);
        verify(repositoryView).showErro();
    }
}